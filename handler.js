'use strict';

const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  accessKeyId: process.env.ACCESS_KEY,
  secretAccessKey: process.env.SECRET_ACCESS_KEY,
  region: process.env.REGION
});
const fileType = require('file-type');

module.exports.create = (event, context, callback) => {
  let request = JSON.parse(event.body);
  let base64String = request.imgString;
  let buffer = Buffer.from(base64String, 'base64'); // pass the base64 string into buffer
  let fileMime = fileType(buffer);
  if (fileMime === null) { // check if the encoded string is a file
    return context.fail('Invalid file type');
  }
  let file = getFile(fileMime, buffer);
  let params = file.params;

  s3.upload(params, (err, data) => {
    if (err)
      callback(err, null);
    else {
      let responseBody = {
        message: 'File URL - ' + file.uploadFile
      };
      let response = {
        statusCode: 200,
        body: JSON.stringify(responseBody)
      };
      callback(null, response);
    }
  });

}

let getFile = function (fileMime, buffer) {
  let fileExt = fileMime.ext; // get file extension
  let hash = new Date().getTime();

  let fileName = hash + '.' + fileExt;
  let fileFullName = fileName;
  let fileFullPath = process.env.CLOUDFRONT + '/' + fileFullName;
  let params = {
    Bucket: process.env.BUCKET_NAME,
    Key: fileFullName,
    Body: buffer,
    ContentType: fileMime.mime,
    ACL: 'public-read'
  };

  return {
    'params': params,
    'uploadFile': fileFullPath
  };
}